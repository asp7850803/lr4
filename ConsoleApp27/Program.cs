﻿using System.Reflection;

Car electricCar = new Car("Model S", "Tesla", 2022, 80000.0, true);
Car gasolineCar = new Car("Civic", "Honda", 2023, 25000.0, false);
Type electtricCarType = electricCar.GetType();
TypeInfo electtricCarTypeInfo = electtricCarType.GetTypeInfo();

Console.WriteLine($"Type Name: {electtricCarTypeInfo.Name}");
Console.WriteLine($"Is Class: {electtricCarTypeInfo.IsClass}");
Console.WriteLine($"Is Abstract: {electtricCarTypeInfo.IsAbstract}");
Console.WriteLine($"Is Sealed: {electtricCarTypeInfo.IsSealed}");

Console.WriteLine("Constructors:");
var constructors = electtricCarTypeInfo.GetConstructors();
foreach (var constructor in constructors)
{
    Console.WriteLine(constructor.Name);
}
Console.WriteLine("Methods: ");
var methods = electtricCarTypeInfo.GetMethods();
foreach (var method in methods)
{
    Console.WriteLine(method.Name);
}
Console.WriteLine("////////////////////////////////");
MemberInfo[] members = electtricCarType.GetMembers();
Console.WriteLine("Members:");
foreach (var member in members)
{
    Console.WriteLine(member.Name);
}
Console.WriteLine("////////////////////////////////");
FieldInfo[] fields = electtricCarType.GetFields();
Console.WriteLine("Fields:");
foreach (var field in fields)
{
    Console.WriteLine(field.Name);
}
Console.WriteLine("////////////////////////////////");
MethodInfo methodCountPriceUahInfo = electtricCarType.GetMethod("CountPriceUah");
if (methodCountPriceUahInfo != null)
{
    object[] parameters = { electricCar };
    double result = (double)methodCountPriceUahInfo.Invoke(electricCar, parameters);
    Console.WriteLine(result);
}
Console.WriteLine(methodCountPriceUahInfo);
MethodInfo methodDisplayInfoInfo = electtricCarType.GetMethod("DisplayInfo");
Console.WriteLine(methodDisplayInfoInfo);
MethodInfo methodComparePriceInfo = electtricCarType.GetMethod("ComparePrice");
Console.WriteLine(methodComparePriceInfo);

internal class Car
{
    public string brand = "";
    protected string model="";
    internal int year;
    private double price;
    protected internal bool isElectric;

    public Car (string model, string brand, int year, double price, bool isElectric)
    {
        this.price = price;
        this.model = model;
        this.brand = brand;
        this.year = year;
        this.isElectric = isElectric;
    }

    public double CountPriceUah(Car car)
    {
        return car.price*36.7;
    }

    public void DisplayInfo()
    {
        Console.WriteLine($"Model: {model}");
        Console.WriteLine($"Brand: {brand}");
        Console.WriteLine($"Year: {year}");
        Console.WriteLine($"Price: {price:$}");
        Console.WriteLine($"Is Electric: {isElectric}");
    }

    public void ComparePrice(Car firstCar, Car secondCar)
    {
        string result=firstCar.price > secondCar.price ? "Price of the first car is bigger" : "Price of the second car is bigger";
        Console.WriteLine(result);
    }
}

